set(aevasession_rpaths)
if (APPLE AND USE_SYSTEM_qt5)
  # On macOS, Qt5 packages use `@rpath` as their library ids. Add an rpath for
  # it to the build.
  list(APPEND aevasession_rpaths
    "${qt5_rpath}")
endif ()
string(REPLACE ";" "${_superbuild_list_separator}"
  aevasession_rpaths
  "${aevasession_rpaths}")

superbuild_add_project(aevasession
  DEVELOPER_MODE
  DEBUGGABLE
  DEPENDS boost cxx11 libarchive paraview qt5 smtk vtk itk itkvtkglue
  DEPENDS_OPTIONAL python python2 python3
  CMAKE_ARGS
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DITK_DIR:PATH=${itk_install_dir}
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -DCMAKE_INSTALL_RPATH:STRING=${aevasession_rpaths}
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib)
