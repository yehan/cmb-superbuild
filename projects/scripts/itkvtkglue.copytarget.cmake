# copies ITKVtkGlueTargets.cmake from ITK build directory to where
# ITK should install it in the sb install directory
file(READ "${itk_build_dir}/itk/build/lib/cmake/ITK-5.1/Modules/Targets/ITKVtkGlueTargets.cmake" contents)

# Replace the build path with the install path.
string(REPLACE "${itk_build_dir}/itk/build/" "${install_location}/" contents "${contents}")

file(WRITE "${install_location}/lib/cmake/ITK-5.1/Modules/Targets/ITKVtkGlueTargets.cmake" "${contents}")
